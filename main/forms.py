from django import forms
from django.utils import timezone
from .models import ROLE_CHOICES, ROLE_MR_X, User, Action


class LoginForm(forms.Form):
    name = forms.CharField(
        max_length=128,
        label="Name",
        help_text="The name of the player / team",
    )
    role = forms.ChoiceField(
        choices=ROLE_CHOICES,
        label="Role",
        help_text="The role of the player / team",
    )

    def clean(self) -> dict:
        cleaned_data = super().clean()
        name = cleaned_data["name"]
        role = cleaned_data["role"]

        try:
            existing_user = User.objects.get(name=name)
            if existing_user.role == role:
                return cleaned_data
            else:
                raise forms.ValidationError(
                    "User with same name but different role already exists."
                )
        except User.DoesNotExist:
            if role == ROLE_MR_X and User.objects.filter(role=ROLE_MR_X).exists():
                raise forms.ValidationError("Rolle Mr. X ist schon vergeben.")
            return cleaned_data

    def save(self, commit=True) -> User:
        name = self.cleaned_data["name"]
        role = self.cleaned_data["role"]
        try:
            existing_user = User.objects.get(name=name)
            if existing_user.role == role:
                return existing_user
            else:
                raise forms.ValidationError(
                    "User with same name but different role already exists."
                )
        except User.DoesNotExist:
            return User.objects.create(
                name=name,
                role=role,
            )


class NewActionForm(forms.ModelForm):
    class Meta:
        model = Action
        fields = ["stop", "mean_of_transport", "black_ticket"]

    def __init__(self, user: User, round: int, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = user
        self.round = round

        if not user.is_mr_x or not round % 3 == 0:
            # hide
            self.fields["black_ticket"].disabled = True
            self.fields["black_ticket"].widget = forms.widgets.HiddenInput()
        elif user.actions.filter(black_ticket=True).exists():
            # disable
            self.fields["black_ticket"].disabled = True
            self.fields["black_ticket"].label += " (Already used)"

    def clean_black_ticket(self):
        black_ticket = self.cleaned_data["black_ticket"]
        if black_ticket:
            if self.round % 3 != 0:
                raise forms.ValidationError(
                    message="Black Ticket is only allowed in every third round!",
                    code="black-ticket-invalid-round",
                )
            elif not self.user.is_mr_x:
                raise forms.ValidationError(
                    message="Only Mr. X is allowed to use the Black Ticket!",
                    code="black-ticket-agent",
                )
            elif Action.objects.filter(black_ticket=True).exists():
                raise forms.ValidationError(
                    message="Back Ticket was already used!",
                    code="black-ticket-already-used",
                )

        return black_ticket

    def save(self, commit=True) -> Action:
        assert commit

        try:
            action = Action.objects.get(user=self.user, round=self.round)
            action.stop = self.cleaned_data["stop"]
            action.mean_of_transport = self.cleaned_data["mean_of_transport"]
            action.black_ticket = self.cleaned_data["black_ticket"]
            action.time = timezone.now()
            action.save()
            return action
        except Action.DoesNotExist:
            return Action.objects.create(
                user=self.user,
                round=self.round,
                **self.cleaned_data,
            )
