from django.urls import path
from . import views

urlpatterns = [
    path("login.php2", views.login, name="login"),
    path("logout.exe", views.logout, name="logout"),
    path("round.dll", views.round, name="round"),
    path("", views.game, name="game"),
]