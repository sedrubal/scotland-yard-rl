# Generated by Django 4.2.4 on 2023-08-31 21:50

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("main", "0004_action_round_alter_action_mean_of_transport_and_more"),
    ]

    operations = [
        migrations.AddField(
            model_name="action",
            name="black_ticket",
            field=models.BooleanField(
                default=False,
                help_text="Hide the station from the agents?",
                verbose_name="Black Ticket 🏴",
            ),
        ),
        migrations.AlterField(
            model_name="action",
            name="mean_of_transport",
            field=models.CharField(
                choices=[
                    ("ubahn", "U-Bahn 🚇"),
                    ("sbahn", "S-Bahn 🚂"),
                    ("tram", "Tram 🚋"),
                    ("bus", "Bus 🚌"),
                ],
                help_text="The mean of transport, that was used to reach the stop.",
                max_length=5,
                verbose_name="Mean of Transport",
            ),
        ),
        migrations.AlterField(
            model_name="user",
            name="role",
            field=models.CharField(
                choices=[("agent", "👮\u200d♀️ Agent"), ("mr. x", "👤 Mister X")],
                help_text="The role of the player team.",
                max_length=5,
                verbose_name="role",
            ),
        ),
    ]
