from datetime import timedelta
import json
from django.shortcuts import render
from django.forms import ValidationError
from django.http import HttpResponseBadRequest, HttpResponseRedirect
from django.shortcuts import redirect, reverse
from django.core.signing import TimestampSigner
from django.conf import settings

from .models import Action, User, Round

from .forms import LoginForm, NewActionForm

signer = TimestampSigner(salt=settings.FRICKEL_SALT)


def login(request):
    if request.method == "GET":
        form = LoginForm()
        return render(
            request,
            "scotland_yard_rl/login.html",
            {
                "form": form,
            },
        )
    elif request.method == "POST":
        form = LoginForm(data=request.POST)
        if form.is_valid():
            try:
                user = form.save()
                resp = HttpResponseRedirect(reverse("game"))
                cookie_value = signer.sign(user.pk)
                resp.set_cookie("user", value=cookie_value)
                return resp
            except ValidationError as err:
                return HttpResponseBadRequest(str(err))
        else:
            return render(
                request,
                "scotland_yard_rl/login.html",
                {
                    "form": form,
                },
            )
    else:
        return HttpResponseBadRequest("Invalid method")


def logout(request):
    response = HttpResponseRedirect(reverse("login"))
    response.delete_cookie("user")
    return response


def game(request):
    try:
        user_id_signed = request.COOKIES.get("user", None)
        if not user_id_signed:
            raise User.DoesNotExist()
        user_id = signer.unsign(user_id_signed, max_age=timedelta(hours=24))
        user = User.objects.get(pk=user_id)
    except User.DoesNotExist:
        return redirect("login")

    current_round = Round.get_current_round()

    # history
    last_checkins = (
        user.actions.order_by("time")
        .values("stop__name", "stop__lat", "stop__lon")
        .all()
    )
    last_checkins_json = json.dumps(
        [
            {
                "lat": float(entry["stop__lat"]),
                "lon": float(entry["stop__lon"]),
                "name": entry["stop__name"],
            }
            for entry in last_checkins
        ]
    )
    # other player locations
    player_locations = []
    current_round_checkins = Action.objects.filter(round=current_round).count()
    for player in User.objects.all():
        player_last_checkin = player.last_checkin
        if not player_last_checkin:
            continue
        player_locations.append(
            {
                "round": player_last_checkin.round,
                "name": player_last_checkin.stop.name
                if hasattr(player_last_checkin, "stop")
                else "???",
                "lat": float(player_last_checkin.stop.lat)
                if hasattr(player_last_checkin, "stop")
                else "0",
                "lon": float(player_last_checkin.stop.lon)
                if hasattr(player_last_checkin, "stop")
                else "0",
                "color": player.color,
                "player": player.name,
                "ismrx": player.is_mr_x,
            }
        )

    if request.method == "GET":
        new_action_form = NewActionForm(user=user, round=current_round)

        return render(
            request,
            "scotland_yard_rl/game.html",
            {
                "user": user,
                "users": User.objects.all(),
                "new_action_form": new_action_form,
                "last_checkins": last_checkins_json,
                "player_locations": json.dumps(player_locations),
                "round": current_round,
                "current_round_checkins": current_round_checkins,
            },
        )
    elif request.method == "POST":
        new_action_form = NewActionForm(
            user=user,
            round=current_round,
            data=request.POST,
        )
        if new_action_form.is_valid():
            new_action_form.save()
            return redirect("/")
        else:
            return render(
                request,
                "scotland_yard_rl/game.html",
                {
                    "user": user,
                    "users": User.objects.all(),
                    "new_action_form": new_action_form,
                    "last_checkins": last_checkins_json,
                    "player_locations": json.dumps(player_locations),
                    "round": current_round,
                    "current_round_checkins": current_round_checkins,
                },
            )
    else:
        return HttpResponseBadRequest("Invalid method")


def round(request):
    current_round = Round.get_current_round()
    if request.method == "GET":
        return render(request, "scotland_yard_rl/round.html", {"round": current_round})
    elif request.method == "POST":
        if "inc" in request.POST.keys():
            Round.increment_round()
        elif "dec" in request.POST.keys():
            Round.decrement_round()
        elif "reset" in request.POST.keys():
            Action.objects.all().delete()
            User.objects.all().delete()
        else:
            return HttpResponseBadRequest("Invalid action")

        return redirect("round")
    else:
        return HttpResponseBadRequest("Invalid method")
