import hashlib
from django.db import models


ROLE_MR_X = "mr. x"
ROLE_AGENT = "agent"
ROLE_CHOICES = [
    (ROLE_AGENT, "👮‍♀️ Agent"),
    (ROLE_MR_X, "👤 Mister X"),
]

MEAN_OF_TRANSPORT = [
    ("ubahn", "U-Bahn 🚇"),
    ("sbahn", "S-Bahn 🚂"),
    ("tram", "Tram 🚋"),
    ("bus", "Bus 🚌"),
]
MEAN_OF_TRANSPORT_LOOKUP = dict(MEAN_OF_TRANSPORT)


class User(models.Model):
    name = models.CharField(
        verbose_name="name",
        primary_key=True,
        max_length=128,
        help_text="The name of the player team.",
    )
    role = models.CharField(
        verbose_name="role",
        choices=ROLE_CHOICES,
        max_length=5,
        help_text="The role of the player team.",
    )

    def __str__(self) -> str:
        return f"<{self.name} ({self.role})>"

    @property
    def last_checkin(self) -> "Action | None":
        current_round = Round.get_current_round()
        for round in range(current_round - 1, 0, -1):
            try:
                lc: Action | None = self.actions.filter(round=round).last()
                if not lc:
                    continue
                if self.is_mr_x:
                    if lc.black_ticket:
                        lc.mean_of_transport = "xxx"
                    if (current_round - 1) % 3 != 0:
                        lc.stop = None
                return lc
            except Action.DoesNotExist:
                continue
        return None

    @property
    def is_mr_x(self) -> bool:
        return self.role == ROLE_MR_X

    @property
    def has_checked_in(self) -> bool:
        current_round = Round.get_current_round()
        return self.actions.filter(round=current_round).exists()

    @property
    def color(self) -> str:
        if self.is_mr_x:
            return "#000"
        color_map = [
            "#16A085",
            "#1ABC9C",
            "#27AE60",
            "#2980B9",
            "#2C3E50",
            "#2ECC71",
            "#3498DB",
            "#8E44AD",
            "#9B59B6",
            "#C0392B",
            "#D35400",
            "#E67E22",
            "#E74C3C",
            "#F1C40F",
            "#F39C12",
            "#FF5733",
        ]

        sha256_hash = hashlib.sha256(self.name.encode()).hexdigest()
        hash_int = int(sha256_hash, 16)
        index = hash_int % len(color_map)
        return color_map[index]


class Stop(models.Model):
    id = models.CharField(
        verbose_name="ID",
        max_length=32,
        primary_key=True,
        help_text="The VGN Station ID",
    )
    name = models.CharField(
        verbose_name="Name of the Station",
        max_length=128,
        help_text="The name of the station",
    )
    lat = models.DecimalField(
        verbose_name="Latitude of the location",
        max_digits=15,
        decimal_places=13,
        help_text="Latitude of the location (roughly)",
    )
    lon = models.DecimalField(
        verbose_name="Longitude of the location",
        max_digits=15,
        decimal_places=13,
        help_text="Longitude of the location (roughly)",
    )

    def __str__(self) -> str:
        return self.name


class Action(models.Model):
    time = models.DateTimeField(
        verbose_name="Date Time",
        auto_now_add=True,
        help_text="When was this checkin?",
    )
    round = models.IntegerField(
        verbose_name="round",
        help_text="In which round was this checkin?",
    )
    user = models.ForeignKey(
        verbose_name="user",
        to=User,
        on_delete=models.CASCADE,
        related_name="actions",
        help_text="The user who made this action.",
    )
    stop = models.ForeignKey(
        verbose_name="Stop",
        to=Stop,
        on_delete=models.CASCADE,
        related_name="check_ins",
        help_text="The stop where the user was.",
    )
    mean_of_transport = models.CharField(
        verbose_name="Mean of Transport",
        choices=MEAN_OF_TRANSPORT,
        max_length=5,
        help_text="The mean of transport, that was used to reach the stop.",
    )
    black_ticket = models.BooleanField(
        verbose_name="Black Ticket 🏴",
        default=False,
        help_text="Hide the station from the agents?",
    )

    @property
    def mean_of_transport_human(self) -> str | None:
        if not self.mean_of_transport:
            return None
        elif self.mean_of_transport in MEAN_OF_TRANSPORT_LOOKUP:
            return MEAN_OF_TRANSPORT_LOOKUP[self.mean_of_transport]
        else:
            return self.mean_of_transport

    def __str__(self) -> str:
        return f"{self.user}: {self.mean_of_transport} -> {self.stop}"


class Round(models.Model):
    current_round = models.IntegerField(
        verbose_name="current round",
    )
    last_updated = models.DateTimeField(
        verbose_name="Last updated",
        auto_now=True,
    )

    @classmethod
    def get_current_round_obj(cls) -> "Round":
        obj = Round.objects.first()
        if not obj:
            obj = Round.objects.create(current_round=1)
        return obj

    @classmethod
    def get_current_round(cls) -> int:
        obj = cls.get_current_round_obj()
        return obj.current_round

    @classmethod
    def increment_round(cls) -> int:
        obj = cls.get_current_round_obj()
        obj.current_round += 1
        obj.save()
        return obj.current_round

    @classmethod
    def decrement_round(cls) -> int:
        obj = cls.get_current_round_obj()
        obj.current_round -= 1
        if obj.current_round <= 0:
            obj.current_round = 1
        obj.save()
        return obj.current_round
