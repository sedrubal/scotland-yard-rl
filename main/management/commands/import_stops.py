import csv
from pathlib import Path

from django.core.management.base import BaseCommand

from ...models import Stop

BOUND_LAT_NORTH = 49.5291282936255
BOUND_LAT_SOUTH = 49.3550435983718
BOUND_LON_EAST = 11.1998985199175
BOUND_LON_WEST = 10.9947412753017


class Command(BaseCommand):
    help = "Import VGN Stops."

    def add_arguments(self, parser):
        parser.add_argument("file", type=Path)

    def handle(self, *args, **options):
        counter = 0
        normalize_name_lookup = {
            "Dambach Alte Veste": "Dambach/FÜ Alte Veste",
            "Nürnberg, Katzwang Bahnhof": "Nürnberg Katzwang Bahnhof",
            "Nürnberg Katzwang": "Nürnberg Katzwang Bahnhof",
            "Sachsen (b.Ansbach)": "Sachsen (b.Ansbach) Bahnhof",
            "Sachsen (b.Ansbach) Bf": "Sachsen (b.Ansbach) Bahnhof",
            "Schnaittach Markt": "Schnaittach Markt / Bahnhof",
            "Schnaittach Bahnhof": "Schnaittach Markt / Bahnhof",
            "Simmelsdorf-Hüttenbach": "Simmelsdorf-Hüttenbach Bahnhof",
            "Simmelsd.-Hüttenbach Bf": "Simmelsdorf-Hüttenbach Bahnhof",
            "Kleinsteinach/Riedbach Ort": "Kleinsteinach/Riedbach Ort / Obere Dorfstr.",
            "Kleinsteinach Obere Dorfstr.": "Kleinsteinach/Riedbach Ort / Obere Dorfstr.",
            "Iphofen Busbahnhof": "Iphofen",
        }

        with options["file"].open("r") as file:
            reader = csv.DictReader(file)
            for entry in reader:
                # stop_id,stop_name,stop_lat,stop_lon,location_type,parent_station
                stop_id = entry["stop_id"]
                name = entry["stop_name"].strip().replace("  ", " ")
                lat = entry["stop_lat"]
                lon = entry["stop_lon"]
                parent_id = entry["parent_station"]
                if stop_id.startswith("Parent"):
                    normalized_id = stop_id.replace("Parent", "")
                else:
                    parts = stop_id.split(":")
                    assert len(parts) >= 3
                    assert parts[0], "de"
                    try:
                        int(parts[1])
                        int(parts[2])
                    except ValueError:
                        raise AssertionError(f"{parts[1]} and {parts[2]} must ints.")

                    normalized_id = ":".join(parts[0:3])

                if name in normalize_name_lookup:
                    name = normalize_name_lookup[name]

                lat_float = float(lat)
                lon_float = float(lon)
                if not BOUND_LAT_NORTH > lat_float > BOUND_LAT_SOUTH:
                    continue
                if not BOUND_LON_WEST < lon_float < BOUND_LON_EAST:
                    continue

                if parent_id:
                    # prefer parent station
                    continue
                elif Stop.objects.filter(name=name).exists():
                    # skip duplicate entry
                    continue
                elif Stop.objects.filter(id=normalized_id).exists():
                    existing_name = Stop.objects.get(id=normalized_id).name
                    if existing_name == f"{name} Bahnhof":
                        continue
                    elif name == f"{existing_name} Bahnhof":
                        existing_stop = Stop.objects.get(id=normalized_id)
                        existing_stop.name = name
                        existing_stop.save()
                        continue
                    self.stdout.write(
                        self.style.ERROR(
                            f"Skipping station with different name but same ID. ID: {normalized_id}, old name: {existing_name}, new name: {name}"
                        )
                    )
                    breakpoint()
                    continue
                self.stdout.write(self.style.MIGRATE_LABEL(f"Importing {name}..."))
                Stop.objects.create(
                    id=normalized_id,
                    name=name,
                    lat=lat,
                    lon=lon,
                )
                counter += 1

        self.stdout.write(self.style.SUCCESS(f"Successfully imported {counter} stops."))
